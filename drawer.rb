# frozen_string_literal: true

require 'mini_magick'

# add blank? to String
class String
  def blank?
    strip.empty?
  end
end

# draw text on image
class Drawer
  LINE1_ONE_X = 50
  LINE1_TWO_X = 46

  LINE2_ONE_X = 264
  LINE2_TWO_X = 258

  START_Y = 580
  TOTAL_HEIGHT = 260

  attr_reader :base_image

  FONT = '/tmp/000.ttf'

  def initialize(image, font)
    @base_image = MiniMagick::Image.read(image.get.body)
    File.open(FONT, 'wb') do |f|
      f.puts(font.get.body.read)
    end
  end

  def draw(text)
    return if text.blank?

    # check if saved in redis

    image = draw_text(text.split("\n"))
    image_data = image.to_blob

    image.resize('256x256')
    thumb_data = image.to_blob

    [image_data, thumb_data]
  end

  private

  def draw_text(lines)
    line1, line2, line3 = lines
    image = base_image

    draw_multiline(image, line1, LINE1_ONE_X, LINE1_TWO_X) if line1
    draw_multiline(image, line2, LINE2_ONE_X, LINE2_TWO_X) if line2

    draw_footer(image, line3) if line3
    image
  end

  def draw_multiline(image, line, one_x, two_x)
    if line.length < 9
      draw_line(image, line, 30, one_x)
    elsif line.length < 11
      draw_line(image, line, 24, one_x + 3)
    else
      ellipsis = line.length > 18
      draw_line(image, line[0, 10], 24, two_x)
      draw_line(image, "#{line[10, 8]}#{ellipsis ? ' ' : ''}", 24, two_x + 25, ellipsis: ellipsis)
    end
  end

  def draw_line(image, text, pointsize, x, ellipsis: false)
    y = start_y(pointsize, text.size)
    image.combine_options do |i|
      i.font FONT
      i.gravity 'northeast'
      i.pointsize pointsize

      last_y = y
      text.split('').each_with_index do |char, idx|
        next if char.blank?

        char_x = char.ascii_only? ? x + ascii_offset(pointsize) : x
        char_y = y + pointsize * idx
        last_y = char_y
        i.draw "text #{char_x},#{char_y} '#{char}'"
      end

      draw_ellipsis(image, x + ascii_offset(pointsize), last_y + 10) if ellipsis
    end
  end

  def draw_footer(image, text)
    image.combine_options do |i|
      i.font FONT
      i.gravity 'southeast'
      i.pointsize 16
      i.draw "text 40,16 '*#{text}'"
    end
  end

  def draw_ellipsis(image, x, y)
    3.times do |idx|
      image.combine_options do |i|
        i.font FONT
        i.gravity 'northeast'
        i.pointsize 30
        i.draw "text #{x},#{y + idx * 10} '.'"
      end
    end
  end

  def ascii_offset(pointsize)
    pointsize / 4
  end

  def start_y(pointsize, count)
    START_Y + (TOTAL_HEIGHT - pointsize * count) / 2
  end
end
