#!/usr/bin/env bash

set -x
curl --request POST --url "https://api.telegram.org/bot$1/setWebhook" --header 'content-type: application/json' --data "{\"url\": \"$2\"}"
