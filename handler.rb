# frozen_string_literal: true

load 'vendor/bundle/bundler/setup.rb'

require 'aws-sdk-s3'
require 'json'
require 'telegram/bot'
require 'telegram/bot/types'

require_relative './drawer.rb'

BUCKET = ENV['BUCKET']
HELP_MSG = %{
发送2句或3句，用回车隔开。例如：
`学校的同意
我才不认可`
或者
`学校的同意
我才不认可
他们确实这样说过
`
}

Telegram.bots_config = {
  default: ENV['TG_TOKEN']
}

def hello(event:, context:)
  body = JSON.parse(event['body'])
  update = Telegram::Bot::Types::Update.new(body)
  if update.message
    answer_message(update.message)
  elsif update.inline_query
    answer_inline_query(update.inline_query)
  end
  { statusCode: 200 }
end

def answer_message(msg)
  p 'answering message'
  return if msg.text.nil? || msg.text.empty?

  if msg.text.start_with?('/start') || msg.text.start_with?('/help')
    return Telegram.bot.send_message(
      chat_id: msg.chat.id,
      parse_mode: 'markdown',
      text: HELP_MSG
    )
  end

  image_url, = generate_images(msg.text)
  Telegram.bot.send_photo(
    chat_id: msg.chat.id,
    photo: image_url
  )
rescue Telegram::Bot::Error => e
  p e
end

def answer_inline_query(query)
  p 'answering inline query'
  return if query.query.nil? || query.query.empty?

  image_url, thumb_url = generate_images(query.query)
  p image_url, thumb_url
  Telegram.bot.answer_inline_query(
    inline_query_id: query.id,
    results: [{ type: 'photo', id: image_url.hash.to_s,
                photo_url: image_url, thumb_url: thumb_url }]
  )
rescue Telegram::Bot::Error => e
  p e
end

def generate_images(text)
  s3 = Aws::S3::Resource.new
  image_key, thumb_key = image_paths(text)
  image = s3.bucket(BUCKET).object(image_key)
  thumbnail = s3.bucket(BUCKET).object(thumb_key)

  unless image.exists?
    base_image = s3.bucket(BUCKET).object('perform-again/assets/000.jpg')
    font = s3.bucket(BUCKET).object('perform-again/assets/000.ttf')

    drawer = Drawer.new(base_image, font)
    image_data, thumb_data = drawer.draw(text)

    image.put(acl: 'public-read', body: image_data)
    thumbnail.put(acl: 'public-read', body: thumb_data)
  end

  [image.public_url, thumbnail.public_url]
end

def image_paths(text)
  hash = ::Digest::MD5.hexdigest(text)
  ["perform-again/public/#{hash}.png",
   "perform-again/public/#{hash}-thumb.png"]
end
